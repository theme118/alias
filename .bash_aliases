alias r='kwin-restart'

alias start_plasma="kstart5 plasmashell"
alias restart_plasma="kquitapp5 plasmashell && kstart5 plasmashell"
alias l='ls -l'
alias ll='ls -lart'
alias cls='clear'
alias findh='grep -rnw . -e'